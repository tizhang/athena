/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/
#ifndef MUONGEOMODELR4_MUONDETECTORDEFS_H
#define MUONGEOMODELR4_MUONDETECTORDEFS_H

#include <GeoPrimitives/GeoPrimitives.h>
///
#include <ActsGeoUtils/ArrayHelper.h>
#include <ActsGeoUtils/SurfaceBoundSet.h>
#include <ActsGeoUtils/AlgebraHelpers.h>
#include <ActsGeoUtils/StringUtils.h>
#include <ActsGeoUtils/BitUtils.h>

#include <GeoPrimitives/GeoPrimitivesHelpers.h>
#include <EventPrimitives/EventPrimitivesToStringConverter.h>
#include <ActsGeometryInterfaces/ActsGeometryContext.h>
#include <ActsGeometryInterfaces/RawGeomAlignStore.h>


#include <GeoModelKernel/GeoVAlignmentStore.h>
#include <Identifier/Identifier.h>
#include <Identifier/IdentifierHash.h>

#include <functional>

//// This header contains common helper utilities and definitions
namespace MuonGMR4 {   
 
}  // namespace MuonGMR4

#endif